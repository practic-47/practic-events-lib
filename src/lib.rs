#[macro_use]
extern crate serde_derive;
extern crate serde_json;

use std::{convert::TryFrom, fmt};

#[derive(Deserialize, fmt::Debug, Serialize)]
pub struct TelegramNotification {
    pub chat_id: i64,
    pub message: String,
}

impl fmt::Display for TelegramNotification {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({})", self.chat_id)
    }
}

impl TryFrom<Vec<u8>> for TelegramNotification {
    type Error = serde_json::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        serde_json::from_slice(&value)
    }
}

impl TryFrom<TelegramNotification> for Vec<u8> {
    type Error = serde_json::Error;

    fn try_from(value: TelegramNotification) -> Result<Self, Self::Error> {
        serde_json::to_vec(&value)
    }
}

#[derive(Serialize, Deserialize)]
pub struct Payslip {
    pub sender_id: u32,
    pub receiver_id: u32,
    pub amount: u32,
    pub currency: String,
}

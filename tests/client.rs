#[cfg(test)]
mod tests {
    use practic_events_lib::*;
    use std::convert::{TryFrom, TryInto};

    #[test]
    fn test_telegram_notification_into_vec() {
        let expected_vector: Vec<u8> = vec![
            123, 34, 99, 104, 97, 116, 95, 105, 100, 34, 58, 52, 49, 48, 51, 48, 57, 48, 44, 34,
            109, 101, 115, 115, 97, 103, 101, 34, 58, 34, 84, 101, 115, 116, 46, 34, 125,
        ];
        let tg_notification = TelegramNotification {
            chat_id: 4103090,
            message: "Test.".to_string(),
        };
        let actual_result: Vec<u8> = tg_notification
            .try_into()
            .expect("Failed to convert into vector.");
        assert_eq!(expected_vector, actual_result);
    }

    #[test]
    fn test_try_telegram_notification_from_correct_vector() {
        let vector: Vec<u8> = vec![
            123, 34, 99, 104, 97, 116, 95, 105, 100, 34, 58, 52, 49, 48, 51, 48, 57, 48, 44, 34,
            109, 101, 115, 115, 97, 103, 101, 34, 58, 34, 84, 101, 115, 116, 46, 34, 125,
        ];
        let result = TelegramNotification::try_from(vector);
        assert!(result.is_ok());
    }

    #[test]
    fn test_try_telegram_notification_from_incorrect_vector() {
        let vector: Vec<u8> = vec![
            123, 34, 99, 104, 97, 116, 95, 105, 100, 34, 58, 52, 49, 48, 51, 48, 57, 48, 44, 34,
            109,
        ];
        let result = TelegramNotification::try_from(vector);
        assert!(result.is_err());
    }
}

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.4] - 2020-02-14

### Added

### Changed 

- `TelegramNotification#to_vec` replaced with `TryFrom` trait impl.

### Deprecated

### Removed

### Fixed

### Security

## [0.1.3] - 2020-02-09

### Added

### Changed 

- All tests moved to a separate lib to emulate client's behaviour.
- `TelegramNotification#to_vec` made public to be available on client's side. 

### Deprecated

### Removed

### Fixed

### Security

## [0.1.2] - 2020-02-09

### Added

- `Display` trait impl for `TelegramNotification` which displays only *safe* information.
- `Debug` trait derive for `TelegramNotification` which displays all information.
- `to_vec()` method for `TelegramNotification` to remove boilerplate in client projects.

### Changed 

### Deprecated

### Removed

### Fixed

### Security

## [0.1.1] - 2020-02-08

### Added

- `TryFrom` trait impl for `TelegramNotification` to remove boilerplate in client projects.

### Changed 

### Deprecated

### Removed

### Fixed

### Security

## [0.1.0] - 2019-11-18

### Added

- Initial project structure and CI configuration.
- TelegramNotification model.
- Payslip model.

### Changed 

### Deprecated

### Removed

### Fixed

### Security

